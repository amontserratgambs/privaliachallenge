//
//  UIInitializer.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import UIKit

class UIInitializer: NSObject {
    static func getMainViewController() -> UIViewController {
        let vc = MoviesViewController(viewModel: MoviesViewModel(with: AppRestClient()))
        let navController = UINavigationController(rootViewController: vc)
        return navController;
    }
}
