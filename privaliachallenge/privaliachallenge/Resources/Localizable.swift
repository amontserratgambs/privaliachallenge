import Foundation

extension String {

	static var movies: String { return NSLocalizedString("movies", comment: "Movies") }
	static var error_loading_movies: String { return NSLocalizedString("error_loading_movies", comment: "Error loading movies...") }
	static var retry: String { return NSLocalizedString("retry", comment: "Retry") }
	static var search_movies: String { return NSLocalizedString("search_movies", comment: "Search Movies") }
    
}
