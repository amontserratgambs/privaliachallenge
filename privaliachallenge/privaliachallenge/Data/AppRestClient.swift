//
//  AppRestClient.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class AppRestClient : RestManager, DataProvidable {
    
    private static let apiKey = "93aea0c77bc168d8bbce3918cefefa45"
    
    private static let getFeaturedMoviesUrl = "/discover/movie"
    private static let getMoviesByQueryUrl = "/search/movie"

    // MARK: - Featured
    
    func getFeaturedMovies(for page: Int, completeHandler: @escaping (Page?, Int) -> Void) -> DataRequest {
        
        let completeURL = String(format: AppRestClient.getFeaturedMoviesUrl)
        
        let params: [String: Any] = ["page": page,
                                     "sort_by": "popularity.desc"]
        
        return AppRestClient.jsonRequestGET(urlService: completeURL, parameters: params) { (dataRequest) in
            dataRequest.responseObject { (response: DataResponse<Page>) in
                if let error = response.error, (error as NSError).code == -999 { return } //Cancelled request
                completeHandler(response.value, response.response?.statusCode ?? 0)
            }
        }
        
    }
    
    // MARK: - Search
    
    func getMovies(with query: String, for page: Int, completeHandler: @escaping (Page?, Int) -> Void) -> DataRequest {
        
        let completeURL = String(format: AppRestClient.getMoviesByQueryUrl)
        
        let params: [String: Any] = ["page": page,
                                     "query": query]
        
        return AppRestClient.jsonRequestGET(urlService: completeURL, parameters: params) { (dataRequest) in
            dataRequest.responseObject { (response: DataResponse<Page>) in
                if let error = response.error, (error as NSError).code == -999 { return } //Cancelled request
                completeHandler(response.value, response.response?.statusCode ?? 0)
            }
        }
        
    }
    
    
    // MARK: - Refresh token
    
    override class func refreshToken(completionHandler: @escaping (_ isError: Bool) -> Void) {
        completionHandler(false)
    }
    
    override class func configureParams(parameters: Parameters) -> Parameters {
        var params = parameters
        params["api_key"] = apiKey
        return params
    }
    
    override class func configureHeaders() -> [String:String]  {
        let headers = [String:String]()
        //headers["Content-Type"] = "application/json;charset=utf-8"
        return headers
    }
    
}
