//
//  DataProvider.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation
import Alamofire

protocol DataProvidable {
    
    func getFeaturedMovies(for page: Int, completeHandler: @escaping (_ page: Page?, _ errorCode: Int) -> Void) -> DataRequest
    func getMovies(with query: String, for page: Int, completeHandler: @escaping (_ page: Page?, _ errorCode: Int) -> Void) -> DataRequest
    
}
