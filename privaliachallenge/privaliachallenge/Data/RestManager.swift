//
//  RestManager.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class RestManager {
    
    static let host = Constants.apiURL
    
    // MARK: - GET
    
    class func jsonRequestGET(urlService: String, parameters: Parameters?, autoRefresh:Bool = true, requestHandler: @escaping (DataRequest) -> Void) -> DataRequest{
        let completeURL = (URL(string: self.host)?.appendingPathComponent(urlService))!
        let headers = self.configureHeaders()
        let params = self.configureParams(parameters: parameters ?? [:])
        
        let request = Alamofire.request(completeURL, method: .get, parameters: params, headers: headers)
        return jsonRequest(request: request, autoRefresh: autoRefresh, requestHandler: requestHandler)
    }
    
    
    // MARK: - POST
    
    class func jsonRequestPOST(urlService: String, parameters: Parameters?, autoRefresh:Bool = true, requestHandler: @escaping (DataRequest) -> Void) -> DataRequest{
        let completeURL = (URL(string: self.host)?.appendingPathComponent(urlService))!
        let headers = self.configureHeaders()
        let params = self.configureParams(parameters: parameters ?? [:])
        
        let request = Alamofire.request(completeURL, method: .post, parameters: params, headers: headers)
        return jsonRequest(request: request, autoRefresh: autoRefresh, requestHandler: requestHandler)
    }
    
    // MARK: - Request
    
    class func jsonRequest(request: DataRequest, autoRefresh:Bool = true, requestHandler: @escaping (DataRequest) -> Void) -> DataRequest{
        return request.response(completionHandler: { (response) in
            if response.response?.statusCode == 401, autoRefresh {
                self.refreshToken() { (isError) in
                    if isError {
                        requestHandler(request)
                    } else {
                        _ = self.jsonRequest(request: request, autoRefresh: false, requestHandler: requestHandler)
                    }
                }
                return
            }
            requestHandler(request)
        })
    }
    
    // MARK: - Refresh token
    
    class func refreshToken(completionHandler: @escaping (_ isError: Bool) -> Void) {
        completionHandler(true)
    }
    
    // MARK: - Configs
    
    class func configureParams(parameters: Parameters) -> Parameters{
        return parameters
    }
    
    class func configureHeaders() -> [String:String] {
        return [String:String]()
    }
    
}
