//
//  MoviesViewController+Config.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation

extension MoviesViewController {

    //Setup view hierarchy
    func setupUI() {
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = .search_movies
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        view.addSubview(tableView)
    }

    //Layout subviews
    func layoutUI() {
        tableView.fillSuperview()
    }

    //Update subviews content and layout constraints
    func updateUI() {

    }

}
