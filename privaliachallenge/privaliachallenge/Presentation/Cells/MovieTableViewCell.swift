//
//  MovieTableViewCell.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieTableViewCell: UITableViewCell {

    var viewModel: MovieTableViewCellPresentable?
    
    lazy var imgView: UIImageView = {
        let imgView = UIImageView(frame: .zero)
        imgView.clipsToBounds = true
        imgView.backgroundColor = .gray
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()
    
    lazy var lblTitle: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.font = UIFont.systemFont(ofSize: 16, weight: .black)
        lbl.numberOfLines = 0
        return lbl
    }()
    
    lazy var lblOverview: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.font = UIFont.systemFont(ofSize: 13, weight: .thin)
        lbl.numberOfLines = 0
        return lbl
    }()
    
    lazy var lineView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor(white: 0, alpha: 0.2)
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(with viewModel: MovieTableViewCellPresentable) {
        self.viewModel = viewModel
        updateUI()
    }
    
    override func layoutSubviews() {
        DispatchQueue.main.async { [weak self] in
            self?.imgView.layer.cornerRadius = (self?.imgView.frame.size.height ?? 1) / 2.0
        }
    }
    
    func setupUI() {
        contentView.addSubview(imgView)
        contentView.addSubview(stackView)
        
        stackView.addArrangedSubview(lblTitle)
        stackView.addArrangedSubview(lblOverview)
        
        contentView.addSubview(lineView)
    }
    
    func layout() {
        imgView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, topConstant: 20, leftConstant: 20, widthConstant: 100)
        imgView.aspectRatio(multiplier: 1)
        
        imgView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -20).isActive = true
        
        stackView.anchor(top: imgView.topAnchor, left: imgView.rightAnchor, right: contentView.rightAnchor, leftConstant: 20, rightConstant: 20)
        let stackBottomConstraint = stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)
        stackBottomConstraint.priority = .defaultLow
        stackBottomConstraint.isActive = true
        
        lineView.anchor(left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, leftConstant: 20, rightConstant: 20, heightConstant: 1)
    }
    
    func updateUI() {
        lblTitle.text = viewModel?.titleString
        lblOverview.text = viewModel?.overviewString
        
        imgView.image = nil
        if let imageUrl = viewModel?.imageUrl {
            imgView.af_setImage(withURL: imageUrl)
        }
    }

}
