//
//  MovieTableViewCellPresentable.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation

protocol MovieTableViewCellPresentable {
    
    var titleString: String { get }
    var overviewString: String { get }
    var imageUrl: URL? { get }
    
}

class MovieTableViewCellViewModel: MovieTableViewCellPresentable {
    
    let movie: Movie
    
    init(with movie: Movie) {
        self.movie = movie
        
    }
    
    var titleString: String {
        var title = movie.title ?? ""
        if let releaseYear = movie.releaseDate?.components(separatedBy: "-").first {
            title = title + " (\(releaseYear))"
        }
        
        return title
    }
    
    var overviewString: String {
        return movie.overview ?? ""
    }
    
    var imageUrl: URL? {
        guard let imagePath = movie.imagePath else { return nil }
        return URL(string: Constants.imageBaseURL + imagePath)
    }
    
}
