//
//  MoviesPresentable.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation
import Alamofire

protocol MoviesViewModelDelegate: class {

    func moviesChanged()
    func moviesDidFailToLoad()

}

protocol MoviesPresentable {

    weak var delegate: MoviesViewModelDelegate? { get set }
    
    var numberOfRows: Int { get }
    func viewModelForMovie(at index: Int) -> MovieTableViewCellPresentable?
    
    func loadNextPage()
    func searchStringDidChange(newString: String)

}

final class MoviesViewModel: MoviesPresentable {

    weak var delegate: MoviesViewModelDelegate?
    let dataProvider: DataProvidable
    
    var currentPage: Page?
    var movies = [Movie]()
    var currentRequest: DataRequest?
    var currentSearchString = ""
    var loadingPage = false
    
    init(with dataProvider: DataProvidable) {
        self.dataProvider = dataProvider
        
    }
    
    func loadNextPage() {
        if loadingPage { return }
        loadingPage = true
        
        if let currentPage = self.currentPage, currentPage.page == currentPage.totalPages {
            loadingPage = false
            return
        }
        
        if currentSearchString.count == 0 {
            currentRequest = dataProvider.getFeaturedMovies(for: (currentPage?.page ?? 0) + 1, completeHandler: handlePageLoaded)
        } else {
            currentRequest = dataProvider.getMovies(with: currentSearchString, for: (currentPage?.page ?? 0) + 1, completeHandler: handlePageLoaded)
        }
    }
    
    func handlePageLoaded(page: Page?, errorCode: Int) {
        guard let page = page, let newMovies = page.movies else {
            loadingPage = false
            delegate?.moviesDidFailToLoad()
            delegate?.moviesChanged()
            return
        }
        movies.append(contentsOf: newMovies)
        delegate?.moviesChanged()
        currentPage = page
        loadingPage = false
    }
    
    func searchStringDidChange(newString: String) {
        currentSearchString = newString
        currentRequest?.cancel()
        currentPage = nil
        loadingPage = false
        movies.removeAll()
        loadNextPage()
    }
    
    var numberOfRows: Int {
        return movies.count
    }
    
    func viewModelForMovie(at index: Int) -> MovieTableViewCellPresentable? {
        guard index < movies.count else { return nil }
        return MovieTableViewCellViewModel(with: movies[index])
    }

}
