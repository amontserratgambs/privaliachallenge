//
//  MoviesViewController.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController {

    var viewModel: MoviesPresentable
    
    lazy var tableView: UITableView = {
        let tableview = UITableView(frame: .zero)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(MovieTableViewCell.self, forCellReuseIdentifier: MovieTableViewCell.className())
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 100
        tableview.separatorStyle = .none
        return tableview
    }()
    
    let searchController = UISearchController(searchResultsController: nil)

    init(viewModel: MoviesPresentable) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        title = .movies

        setupUI()
        layoutUI()
        updateUI()
        
        viewModel.loadNextPage()
    }

}

extension MoviesViewController: MoviesViewModelDelegate {

    func moviesChanged() {
        tableView.reloadData()
    }
    
    func moviesDidFailToLoad() {
        let alert = UIAlertController(title: nil, message: .error_loading_movies, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: .retry, style: .default) { [weak self] (action) in
            self?.viewModel.loadNextPage()
        }
        alert.addAction(retryAction)
        present(alert, animated: true, completion: nil)
    }

}

extension MoviesViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        viewModel.searchStringDidChange(newString: searchBar.text!)
    }
}

extension MoviesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.numberOfRows - 1 {
            viewModel.loadNextPage()
        }
    }
    
}

extension MoviesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.className(), for: indexPath) as! MovieTableViewCell
        if let viewModel = viewModel.viewModelForMovie(at: indexPath.row) {
            cell.config(with: viewModel)
        }
        return cell
    }
    
}
