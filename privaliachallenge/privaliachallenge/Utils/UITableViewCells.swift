//
//  UITableViewCells.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import UIKit

extension UITableViewCell {

    class func className() -> String {
        return String(describing: self)
    }

    class func instanceFromNib() -> UITableViewCell {
        return self.nib().instantiate(withOwner: nil, options: nil)[0] as! UITableViewCell
    }

}
