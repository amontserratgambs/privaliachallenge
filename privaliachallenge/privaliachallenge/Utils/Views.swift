//
//  Views.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import UIKit

extension UIView {

    class func identifier() -> String {
        return String(describing: self)
    }

    class func nib() -> UINib {
        return UINib(nibName: self.identifier(), bundle: nil)
    }

    func addFadeAnimationToLayer(duration: Double = 0.3) {
        let animation = CATransition()
        animation.type = kCATransitionFade
        animation.duration = duration
        self.layer.add(animation, forKey: nil)
    }

    func setAnchorPoint(anchorPoint: CGPoint) {
        var newPoint = CGPoint(x: self.bounds.size.width * anchorPoint.x, y: self.bounds.size.height * anchorPoint.y)
        var oldPoint = CGPoint(x: self.bounds.size.width * self.layer.anchorPoint.x, y: self.bounds.size.height * self.layer.anchorPoint.y)

        newPoint = __CGPointApplyAffineTransform(newPoint, self.transform)
        oldPoint = __CGPointApplyAffineTransform(oldPoint, self.transform)

        var position = self.layer.position
        position.x -= oldPoint.x
        position.x += newPoint.x

        position.y -= oldPoint.y
        position.y += newPoint.y

        self.layer.position = position
        self.layer.anchorPoint = anchorPoint
    }

}
