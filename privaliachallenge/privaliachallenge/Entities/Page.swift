//
//  Page.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation
import ObjectMapper

class Page: Mappable {
    
    var page: Int?
    var totalPages: Int?
    var movies: [Movie]?
    
    required init?(map: Map){ }
    
    func mapping(map: Map) {
        page <- map["page"]
        totalPages <- map["total_pages"]
        movies <- map["results"]
    }
    
}
