//
//  Movie.swift
//  privaliachallenge
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import Foundation
import ObjectMapper

class Movie: Mappable {
    
    var id: String?
    var title: String?
    var imagePath: String?
    var overview: String?
    var releaseDate: String?
    
    required init?(map: Map){ }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        imagePath <- map["poster_path"]
        overview <- map["overview"]
        releaseDate <- map["release_date"]
    }
    
}
