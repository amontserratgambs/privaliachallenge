//
//  privaliachallengeTests.swift
//  privaliachallengeTests
//
//  Created by Albert Montserrat on 16/3/18.
//  Copyright © 2018 Albert Montserrat. All rights reserved.
//

import XCTest
@testable import privaliachallenge

class privaliachallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetFeaturedMovies() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let expectation = XCTestExpectation(description: "Download featured movies")
        
        let client = AppRestClient()
        
        _ = client.getFeaturedMovies(for: 3) { (page, errorCode) in
            
            XCTAssertTrue(errorCode == 200)
            
            XCTAssertTrue(page?.page == 3)
            
            expectation.fulfill()
            
        }
        
        wait(for: [expectation], timeout: 100.0)
        
    }
    
    func testSearchMovies() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let expectation = XCTestExpectation(description: "Download movies from search")
        
        let query = "The"
        let client = AppRestClient()
        
        _ = client.getMovies(with: query, for: 3) { (page, errorCode) in
            
            XCTAssertTrue(errorCode == 200)
            
            XCTAssertTrue(page?.page == 3)
            
            for movie in page?.movies ?? [] {
                XCTAssertTrue(movie.title?.lowercased().contains(query.lowercased()) ?? false)
            }
            
            expectation.fulfill()
            
        }
        
        wait(for: [expectation], timeout: 100.0)
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            
            let expectation = XCTestExpectation(description: "Download movies from search")
            
            let client = AppRestClient()
            
            _ = client.getMovies(with: "The", for: 3) { (page, errorCode) in
                expectation.fulfill()
            }
            
            wait(for: [expectation], timeout: 100.0)
        }
    }
    
}
